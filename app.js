const express = require('express');
// const {json} = require("express");
const app = express();
const port = 3000;

// Configure server so static assets (images here) can be served.
app.use(express.static('public'))

// Declare DB as a global variable! see notes in the class declaration.
const ImagesDb = require('./modules/faux-db');
var imagesDb = new ImagesDb();

app.get('/', (req, res) => {
    res.send('Hello World ! Try GET /api/images');
});

// Route definition for GET /api/images:
app.get("/api/images", (req, res, next) => {
    // Make sure 2 out of 4 parameters minimum have been provided in the request:
    const validParamKeys = ['carName', 'Grade', 'manufactureYear', 'colour'];
    const paramKeysRequested = new Set(Object.keys(req.query));
    const validParamKeysRequested = validParamKeys.filter(x => paramKeysRequested.has(x));
    if (validParamKeysRequested.length < 2) {
        res.json({
            "error": "Specify at least 2 of the following query params: " + validParamKeys.join(', '),
        })
        return;
    }

    // Build a sanitized version of the parameters object for the next step:
    const requestParamsFiltered = validParamKeysRequested
        .filter(key => key in req.query)
        .reduce((obj2, key) => (obj2[key] = req.query[key], obj2), {});

    const images = imagesDb.list(requestParamsFiltered);

    const prefix = 'http://localhost:' + port + '/images/'
    res.json(images.map(image => prefix + image.filename));
});

let server = app.listen(port, () => {
    console.log(`Server running at http://localhost:${port}/`);
})
