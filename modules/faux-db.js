const fs = require('fs');
const path = require('path');

/**
 * class ImageDb
 *
 * IMPORTANT: For this experiment, db is stored within the app.
 * In a real environment the list of existing image would be managed by some
 * Database outside of this app. The creation/update of images will be managed
 * by a custom api endpoint.
 *
 * @type {ImageDb}
 */
module.exports = class ImageDb {

    /**
     * Load all the filenames of all existing images in memory to make query easier later.
     *
     */
    constructor() {
        console.log('Loading DB...');
        const validParamKeys = ['carName', 'Grade', 'manufactureYear', 'colour'];
        this.images = [];
        // Note on readdirSync: use a synchronous function here because we don't
        // want the app to start listening for request if the DB is not loaded.
        fs.readdirSync('./public/images').forEach(filename => {
            if (filename.endsWith('.png')) {
                let segments = path.parse(filename).name.split('-');
                this.images.push({
                    "filename": filename,
                    "carName": segments[0],
                    "Grade": segments[1],
                    "colour": segments[2],
                    "manufactureYear": segments[3],
                });
            }
        });
        console.log(this.images);
        console.log('done!');
    }

    /**
     * Query image db based on criteria
     *
     * @param criteria
     * @returns {{criteria, list: string}}
     */
    list(criteria) {
        return this.images.filter((imageData) => {
            // Iterate through the db image
            // For all of the provided criteria, make sure the image attributes match.
            let match = true;
            for (const [key, value] of Object.entries(criteria)) {
                if (imageData[key] !== value) {
                    match = false;
                    break;
                }
            }
            return match;
        });
    }
}
