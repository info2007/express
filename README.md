# Express

Experiment using express.js to build a REST API

## Getting started

How to get setup this app as of now:

1. git checkout the repo locally and cd into it
2. build the docker image: `docker build -t express-exp .`
3. run the container: `docker run -d -p 3000:3000 --name exp --rm express-exp`
4. visit the app at `http://localhost:3000`

### Routes:

* Search for images: ` GET /api/images`:
    * query string: carName, Grade, manufactureYear, colour
    * response: uri for matching images.
* Upload images: `POST /api/image` _NOT IMPLEMENTED_ - _TODO_
    * parameters: filename, file content
    * response: message "file created"
    * backend process:
        * add authentication check in middleware ?
        * validation: make sure filename is following pattern `[carName]-[Grade]-[colour]-[manufactureYear].png`
        * file saved to the images folder
        * update "db", ie update `imagesDb.images` with the new image.

## Inception

How I have set up from scratch that app. Noted here for training purpose and if one want to set up a new app afresh.

This is a 100% container-approached, this means all the node.js operations are handled by a container. Aiming to be independent of toolings installed on developer machine (well except for docker!)

1. create folder `mkdir express-exp; cd express-exp`
2. init package.json: `docker run --rm --volume $PWD:/usr/src/app -w /usr/src/app node:16.14-alpine3.15 npm init -y`
3. update name and description in that file to something more suitable.
4. install express package: `docker run --rm --volume $PWD:/usr/src/app -w /usr/src/app node:16.14-alpine3.15 npm install express`
5. create a dummy app.js file
6. do the "getting started"

