# Starting from the basic node docker image (out-of-the-box it offered all the require stack for a node.js app)
# Choosing the latest LTS major version, alpine-based to have a smaller image
FROM node:16.14-alpine3.15
WORKDIR /usr/src/app
COPY package*.json ./
RUN npm install
# copy app file(s) after npm install to benefit from docker building cache.
COPY app.js ./
COPY modules ./modules
# Copy Assets
COPY public ./public

EXPOSE 3000

CMD ["node", "app.js"]