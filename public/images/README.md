# README

**Dev notes on that `images` folder**

On a prod environment those images would not be part of the docker image.
(Because in that case, those images are data, not a static asset)
Instead, the code could read and write from an AWS S3 bucket, whose uri and credentials would have to be passed-on to this app by configuration.


